#include QMK_KEYBOARD_H
#include "sendstring_bepo.h"
#include "quantum/keymap_extras/keymap_bepo.h"

// #ifdef SSD1306OLED
//   #include "ssd1306.h"
// #endif

#define _BASE 0
#define _RAISE 1
#define _LOWER 2

#define SFT_ESC  SFT_T(KC_ESC)
#define CTL_BSPC CTL_T(KC_BSPC)
#define ALT_SPC  ALT_T(KC_SPC)
#define SFT_ENT  SFT_T(KC_ENT)

#define KC_ML KC_MS_LEFT
#define KC_MR KC_MS_RIGHT
#define KC_MU KC_MS_UP
#define KC_MD KC_MS_DOWN
#define KC_MB1 KC_MS_BTN1
#define KC_MB2 KC_MS_BTN1

#define RAISE MO(_RAISE)
#define LOWER MO(_LOWER)

enum keymap_layer {
  KL_BASE,
  KL_QWERTY,
  KL_QWERTY_SHIFTED,
  KL_NUMBER_AND_FCT,
  KL_CURSOR_AND_NUMBERS,
  KL_MOUSE_AND_FCT,
  KL_OSL,
};



#ifdef COMBO_ENABLE
enum combos {
  KDF_OP,
  KJK_CP,
  KSD_OA,
  KKL_CA,
  KSF_OB,
  KJL_CB,
  KHJ_CEDIL,
};


const uint16_t PROGMEM kdf_combo[] = {KC_D, KC_F, COMBO_END};
const uint16_t PROGMEM kjk_combo[] = {KC_J, KC_K, COMBO_END};

const uint16_t PROGMEM ksd_combo[] = {KC_S, KC_D, COMBO_END};
const uint16_t PROGMEM kkl_combo[] = {KC_K, KC_L, COMBO_END};


const uint16_t PROGMEM ksf_combo[] = {KC_S, KC_F, COMBO_END};
const uint16_t PROGMEM kjl_combo[] = {KC_J, KC_L, COMBO_END};

const uint16_t PROGMEM khj_combo[] = {KC_H, KC_J, COMBO_END};


combo_t key_combos[COMBO_COUNT] = {
  [KDF_OP] = COMBO(kdf_combo, KC_4),  // (
  [KJK_CP] = COMBO(kjk_combo, KC_5),  // )
  [KSD_OA] = COMBO(ksd_combo, ALGR(KC_X)), // {
  [KKL_CA] = COMBO(kkl_combo, ALGR(KC_C)), // }
  [KSF_OB] = COMBO(ksf_combo, ALGR(KC_4)), // [
  [KJL_CB] = COMBO(kjl_combo, ALGR(KC_5)), // ]
  [KHJ_CEDIL] = COMBO(khj_combo, KC_NUHS), // ç
};
#endif

// https://docs.qmk.fm/#/feature_tap_dance?id=example-4-39quad-function-tap-dance39
typedef enum {
    TD_NONE,
    TD_UNKNOWN,
    TD_SINGLE_TAP,
    TD_SINGLE_HOLD,
    TD_DOUBLE_TAP,
    TD_DOUBLE_HOLD,
    TD_DOUBLE_SINGLE_TAP, // Send two single taps
    TD_TRIPLE_TAP,
     TD_TRIPLE_HOLD
} td_state_t;

typedef struct {
    bool is_press_action;
    td_state_t state;
} td_tap_t;


// td_state_t cur_dance(qk_tap_dance_state_t *state);

// For the x tap dance. Put it here so it can be used in any keymap
// void x_finished(qk_tap_dance_state_t *state, void *user_data);
// void x_reset(qk_tap_dance_state_t *state, void *user_data);


// Tap Dance declarations
enum {
    TD_WBAK_WFWD,
    TD_ESC_ESCX,
    // TD_LT2_SPACE_UNDERSCORE,
    TD_TAB_SHIFT,
};

typedef struct {
  bool shift;
  bool control;
  bool alt;
  bool altgr;
  bool gui;
} indicators_t;


static indicators_t indicators_state = {
  .shift = false,
  .control = false,
  .alt = false,
  .altgr = false,
  .gui = false,
};


#ifdef OLED_ENABLE
// https://javl.github.io/image2cpp/
// Invert image color
// plain bytes
// Vertical 1 bit by Pixel

// #include "logo_base_layer.c"
#include "logo_cursor_layer.c"
#include "logo_function_layer.c"
#include "logo_gaming_layer.c"
#include "logo_mouse_layer.c"
#include "logo_number_layer.c"
// #include "logo_screen_layer.c"
// #include "logo_text_layer.c"
// #include "logo_zoom_layer.c"
// #include "logo_poulpy.c"


void display_text(const char *data) {
  //render_logo_base_layer();
  oled_set_cursor(7, 0);
  oled_write_ln("Xael", false);
  oled_set_cursor(7, 1);
  oled_write_ln(data, false);
  oled_set_cursor(7, 2);
  oled_write_ln(" ", false);
  oled_set_cursor(7, 3);
  oled_write_ln(" ", false);
}

void clear_screen(const char *data) {
  oled_set_cursor(0, 0);
  if (indicators_state.shift == true) {
    oled_write_ln(" SHIFT", false);
  } else {
    oled_write_ln(" ", false);
  }
  oled_set_cursor(0, 1);
  if (indicators_state.gui == true) {
    oled_write_ln(" GUI", false);
  } else {
    oled_write_ln(" ", false);
  }
  oled_set_cursor(0, 2);
  oled_write_ln(" ", false);
  oled_set_cursor(0, 3);
  oled_write_ln(" ", false);
}

 bool oled_task_user(void) {
   // oled_set_brightness(0);
   switch (get_highest_layer(layer_state|default_layer_state)) {
   case KL_BASE:
     clear_screen("");
     break;
   case KL_NUMBER_AND_FCT:
     render_logo_number_layer();
     // display_text("Numbers");
     break;
   case KL_CURSOR_AND_NUMBERS:
     render_logo_cursor_layer();
     // display_text("Cursor & num");
     break;
   case KL_MOUSE_AND_FCT:
     render_logo_mouse_layer();
     // display_text("Mouse & FKeys");
     break;
   case KL_OSL:
     render_logo_function_layer();
     // display_text("Functions OSL");
     break;
   case KL_QWERTY:
     render_logo_gaming_layer();
     // display_text("Functions OSL");
     break;
   case KL_QWERTY_SHIFTED:
     render_logo_gaming_layer();
     oled_set_cursor(7, 0);
     oled_write("S", false);
     // display_text("Functions OSL");
     break;
   default:
     // render_logo_base_layer();
     display_text("Error layer");
     break;
   }

 
#ifdef WPM_ENABLE
  int current_wpm;
  oled_set_cursor(0, 1);
  current_wpm = get_current_wpm();
  char wpm_counter[4];
  wpm_counter[3] = '\0';
  wpm_counter[2] = '0' + current_wpm % 10;
  wpm_counter[1] = '0' + (current_wpm / 10) % 10;
  wpm_counter[0] = '0' + (current_wpm / 100) % 10;
  oled_write(wpm_counter, false);   
#endif 
  return 0;
}
#endif


td_state_t cur_dance(qk_tap_dance_state_t *state) {
    if (state->count == 1) {
        if (state->interrupted || !state->pressed) return TD_SINGLE_TAP;
        // Key has not been interrupted, but the key is still held. Means you want to send a 'HOLD'.
        else return TD_SINGLE_HOLD;
    } else if (state->count == 2) {
        // TD_DOUBLE_SINGLE_TAP is to distinguish between typing "pepper", and actually wanting a double tap
        // action when hitting 'pp'. Suggested use case for this return value is when you want to send two
        // keystrokes of the key, and not the 'double tap' action/macro.
        if (state->interrupted) return TD_DOUBLE_SINGLE_TAP;
        else if (state->pressed) return TD_DOUBLE_HOLD;
        else return TD_DOUBLE_TAP;
    }

    // Assumes no one is trying to type the same letter three times (at least not quickly).
    // If your tap dance key is 'KC_W', and you want to type "www." quickly - then you will need to add
    // an exception here to return a 'TD_TRIPLE_SINGLE_TAP', and define that enum just like 'TD_DOUBLE_SINGLE_TAP'
    if (state->count == 3) {
        if (state->interrupted || !state->pressed) return TD_TRIPLE_TAP;
        else return TD_TRIPLE_HOLD;
    } else return TD_UNKNOWN;
}

/* // Create an instance of 'td_tap_t' for the 'space' tap dance. */
/* static td_tap_t spacetap_state = { */
/*     .is_press_action = true, */
/*     .state = TD_NONE */
/* }; */

/* void space_finished(qk_tap_dance_state_t *state, void *user_data) { */
/*     spacetap_state.state = cur_dance(state); */
/*     switch (spacetap_state.state) { */
/*     case TD_SINGLE_TAP: register_code(KC_SPC); break; */
/*     case TD_SINGLE_HOLD: layer_on(KL_CURSOR_AND_NUMBERS); break; */
/*     case TD_DOUBLE_TAP: register_code(KC_RALT); register_code(KC_SPC); break; */
/*     case TD_DOUBLE_HOLD: layer_on(KL_CURSOR_AND_NUMBERS); break; */
/*       // Last case is for fast typing. Assuming your key is `f`: */
/*       // For example, when typing the word `buffer`, and you want to make sure that you send `ff` and not `Esc`. */
/*       // In order to type `ff` when typing fast, the next character will have to be hit within the `TAPPING_TERM`, which by default is 200ms. */
/*     case TD_DOUBLE_SINGLE_TAP: register_code(KC_SPC); unregister_code(KC_SPC); register_code(KC_SPC); break; */
/*     case TD_NONE: break; */
/*     case TD_UNKNOWN: break;  */
/*     case TD_TRIPLE_TAP: break; */
/*     case TD_TRIPLE_HOLD: layer_on(KL_CURSOR_AND_NUMBERS); break; */

/*     } */
/* } */

/* void space_reset(qk_tap_dance_state_t *state, void *user_data) { */
/*     switch (spacetap_state.state) { */
/*     case TD_SINGLE_TAP: unregister_code(KC_SPC); break; */
/*     case TD_SINGLE_HOLD: layer_off(KL_CURSOR_AND_NUMBERS); break; */
/*     case TD_DOUBLE_TAP: unregister_code(KC_SPC); unregister_code(KC_RALT); break; */
/*     case TD_DOUBLE_HOLD: layer_off(KL_CURSOR_AND_NUMBERS); break; */
/*     case TD_DOUBLE_SINGLE_TAP: unregister_code(KC_SPC); break; */
/*     case TD_NONE: break; */
/*     case TD_UNKNOWN: break;  */
/*     case TD_TRIPLE_TAP: break; */
/*     case TD_TRIPLE_HOLD: layer_off(KL_CURSOR_AND_NUMBERS); break; */
/*     } */
/*     spacetap_state.state = TD_NONE; */
/* } */

// Create an instance of 'td_tap_t' for the 'tab' tap dance.
static td_tap_t tabtap_state = {
    .is_press_action = true,
    .state = TD_NONE
};

void tab_shift_finished(qk_tap_dance_state_t *state, void *user_data) {
    tabtap_state.state = cur_dance(state);
    switch (tabtap_state.state) {
    case TD_SINGLE_TAP: register_code(KC_TAB); break;
    case TD_SINGLE_HOLD: register_code(KC_LSFT); indicators_state.shift = true; break;
    case TD_DOUBLE_TAP: register_code(KC_LSFT); register_code(KC_TAB); break;
    case TD_DOUBLE_HOLD: break;
    case TD_DOUBLE_SINGLE_TAP: break;
    case TD_NONE: break;
    case TD_UNKNOWN: break; 
    case TD_TRIPLE_TAP: break;
    case TD_TRIPLE_HOLD: break;
    }
}

void tab_shift_reset(qk_tap_dance_state_t *state, void *user_data) {
    switch (tabtap_state.state) {
    case TD_SINGLE_TAP: unregister_code(KC_TAB); break;
    case TD_SINGLE_HOLD: unregister_code(KC_LSFT); indicators_state.shift = false; break;
    case TD_DOUBLE_TAP: unregister_code(KC_TAB); unregister_code(KC_LSFT); break;
    case TD_DOUBLE_HOLD: break;
    case TD_DOUBLE_SINGLE_TAP: break;
    case TD_NONE: break;
    case TD_UNKNOWN: break; 
    case TD_TRIPLE_TAP: break;
    case TD_TRIPLE_HOLD: break;
    }
    tabtap_state.state = TD_NONE;
}

// Create an instance of 'td_tap_t' for the 'esc' tap dance.
static td_tap_t esctap_state = {
    .is_press_action = true,
    .state = TD_NONE
};


void esc_lgui_finished(qk_tap_dance_state_t *state, void *user_data) {
    esctap_state.state = cur_dance(state);
    switch (esctap_state.state) {
    case TD_SINGLE_TAP: register_code(KC_ESC); break;
    case TD_SINGLE_HOLD: register_code(KC_LGUI); indicators_state.gui = true; break;
    case TD_DOUBLE_TAP: register_code(KC_LALT); register_code(KC_C); break;
    case TD_DOUBLE_HOLD: break;
    case TD_DOUBLE_SINGLE_TAP: break;
    case TD_NONE: break;
    case TD_UNKNOWN: break; 
    case TD_TRIPLE_TAP: break;
    case TD_TRIPLE_HOLD: break;
    }
}

void esc_lgui_reset(qk_tap_dance_state_t *state, void *user_data) {
    switch (esctap_state.state) {
    case TD_SINGLE_TAP: unregister_code(KC_ESC); break;
    case TD_SINGLE_HOLD: unregister_code(KC_LGUI); indicators_state.gui = false; break;
    case TD_DOUBLE_TAP: unregister_code(KC_C); unregister_code(KC_LALT); break;
    case TD_DOUBLE_HOLD: break;
    case TD_DOUBLE_SINGLE_TAP: break;
    case TD_NONE: break;
    case TD_UNKNOWN: break; 
    case TD_TRIPLE_TAP: break;
    case TD_TRIPLE_HOLD: break;
    }
    esctap_state.state = TD_NONE;
}


// Tap Dance definitions
qk_tap_dance_action_t tap_dance_actions[] = {
  [TD_WBAK_WFWD] = ACTION_TAP_DANCE_DOUBLE(KC_WBAK, KC_WFWD),
  [TD_ESC_ESCX] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, esc_lgui_finished, esc_lgui_reset),
  //[TD_LT2_SPACE_UNDERSCORE] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, space_finished, space_reset),
  [TD_TAB_SHIFT] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, tab_shift_finished, tab_shift_reset)
};




const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  //      /* BÉPO
  //      * +-----------------------------------------+                             +-----------------------------------------+
  //      * | ESC  |   b  |   é  |   p  |   o  |   è  |                             |   ^  |   v  |   d  |   l  |   j  |   =  |
  //      * |------+------+------+------+------+------|                             |------+------+------+------+------+------|
  //      * | TAB  |   b  |   u  |   i  |   e  |   ,  |                             |   c  |   t  |   s  |   r  |   n  |   m  |
  //      * |------+------+------+------+------+------|                             |------+------+------+------+------+------|
  //      * | SHFT |   a  |   y  |   x  |   .  |   k  |                             |   '  |   q  |   g  |   h  |   f  |   w  |
  //      * +------+------+------+------+-------------+                             +-------------+------+------+------+------+


  //     /* Base (qwerty)
  //      * +-----------------------------------------+                             +-----------------------------------------+
  //      * | ESC  |   q  |   w  |   e  |   r  |   t  |                             |   y  |   u  |   i  |   o  |   p  |      |
  //      * |------+------+------+------+------+------|                             |------+------+------+------+------+------|
  //      * | TAB  |   a  |   s  |   d  |   f  |   g  |                             |   h  |   j  |   k  |   l  |   ;  |      |
  //      * |------+------+------+------+------+------|                             |------+------+------+------+------+------|
  //      * | SHFT |   z  |   x  |   c  |   v  |   b  |                             |   n  |   m  |   ,  |   .  |   /  |      |
  //      * +------+------+------+------+-------------+                             +-------------+------+------+------+------+
  //      *               |  [   |   ]  |                                                         |      |      |
  //      *               +-------------+-------------+                             +-------------+-------------+
  //      *                             |      |      |                             |      |      |
  //      *                             |------+------|                             |------+------|
  //      *                             |      |      |                             |      |      |
  //      *                             +-------------+                             +-------------+
  //      *                                           +-------------+ +-------------+
  //      *                                           |      |      | |      |      |
  //      *                                           |------+------| |------+------|
  //      *                                           |      |      | |      |      |
  //      *                                           +-------------+ +-------------+
  //      */
  
  // https://github.com/qmk/qmk_firmware/blob/master/docs/keycodes_basic.md
  // https://github.com/qmk/qmk_firmware/blob/master/docs/keycodes.md
  // https://github.com/qmk/qmk_firmware/blob/master/docs/feature_tap_dance.md
  // https://jayliu50.github.io/qmk-cheatsheet/

  // MT(MOD_RSFT, KC_SPC) -> espace raté trop souvent
  // TD(TD_LT2_SPACE_UNDERSCORE)  -> pas super fiable
  [KL_BASE] = LAYOUT( // Base
               // _______,  _______,  _______,  _______,  _______,  _______,         _______,  _______,  _______,  _______,  _______,  _______,
               BP_DLR,   BP_B,     BP_EACU,  BP_P,     BP_O,     BP_EGRV,                 BP_DCIR,  BP_V,     BP_D,     BP_L,     BP_J,     BP_Z,
               KC_TAB,   BP_A,     BP_U,     BP_I,     BP_E,     BP_COMM,                 BP_C,     BP_T,     BP_S,     BP_R,     BP_N,     BP_M,
               KC_LSFT,  BP_AGRV,  BP_Y,     BP_X,     BP_DOT,   BP_K,                    BP_QUOT,  BP_Q,     BP_G,     BP_H,     BP_F,     BP_W,
                                   KC_NO,    KC_NO,                                           TG(KL_MOUSE_AND_FCT),    TG(KL_NUMBER_AND_FCT),
               MT(MOD_LCTL, KC_SPC),  MT(MOD_RALT, KC_ENT),       TD(TD_TAB_SHIFT),  LT(KL_CURSOR_AND_NUMBERS, KC_SPACE),
                                                    KC_LALT,  TD(TD_ESC_ESCX),            MT(MOD_LSFT, KC_TAB),   OSL(KL_OSL),
                                                       KC_LEAD, KC_NO,                   C(KC_INS),  S(KC_INS)
                ),
  
  [KL_QWERTY] = LAYOUT(
               BP_DLR,   BP_Q,     BP_W,     BP_E,     BP_R,     BP_T,                    BP_Y,     BP_U,     BP_I,     BP_O,     BP_P,     DF(KL_BASE),
               KC_TAB,   BP_A,     BP_S,     BP_D,     BP_F,     BP_G,                    BP_H,     BP_J,     BP_K,     BP_L,     BP_SCLN,  DF(KL_QWERTY_SHIFTED),
               KC_LSFT,  BP_Z,     BP_X,     BP_C,     BP_V,     BP_B,                    BP_N,     BP_M,     BP_COMM,  BP_DOT,   BP_SLSH,  KC_NO,
                                         KC_NO,    KC_NO,                                           KC_TRNS,  KC_TRNS,  
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS
                       ),
  
  [KL_QWERTY_SHIFTED] = LAYOUT(
               DF(KL_BASE), BP_DLR,   BP_Q,     BP_W,     BP_E,     BP_R,     BP_T,                    BP_Y,     BP_U,     BP_I,     BP_O,     BP_P,     
               KC_NO,  KC_TAB,   BP_A,     BP_S,     BP_D,     BP_F,     BP_G,                    BP_H,     BP_J,     BP_K,     BP_L,     BP_SCLN, 
               KC_NO,  KC_LSFT,  BP_Z,     BP_X,     BP_C,     BP_V,     BP_B,                    BP_N,     BP_M,     BP_COMM,  BP_DOT,   BP_SLSH, 
                                         KC_NO,    KC_NO,                                           KC_TRNS,  KC_TRNS,  
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS
                       ),
  
  [KL_NUMBER_AND_FCT] = LAYOUT(  // Numbers & FUNCTION                                                                                                                                      
               RESET,    KC_F1,     KC_F2,   KC_F3,    KC_F4,    KC_F5,                   KC_F6,    KC_F7,     KC_F8,   KC_F9,    KC_F10,   KC_F11,
               KC_TRNS,  S(KC_1),   S(KC_2), S(KC_3),  S(KC_4),  S(KC_5),                 S(KC_6),  S(KC_7),   S(KC_8), S(KC_9),  S(KC_0),  KC_PDOT,
               KC_TRNS,  KC_1,      KC_2,    KC_3,     KC_4,     KC_5,                    KC_6,     KC_7,      KC_8,    KC_9,     KC_0,     KC_EQL,
                                    KC_TRNS, KC_TRNS,                                                          TO(KL_MOUSE_AND_FCT),   KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS
                 ),
  [KL_CURSOR_AND_NUMBERS] = LAYOUT(  // Cursor & numbers                                                      
               DM_RSTP,  S(KC_1),   S(KC_2), S(KC_3),  S(KC_4),  S(KC_5),                 KC_INS,   KC_HOME,   KC_UP,   KC_END,   C(KC_PGUP),  C(BP_W),
               KC_TRNS,  S(KC_6),   S(KC_7), S(KC_8),  S(KC_9),  S(KC_0),                 KC_BSPC,  KC_LEFT,   KC_DOWN, KC_RGHT,  C(KC_PGDN),  A(KC_9),
               BP_PERC,  KC_7,      KC_8,    KC_9,     KC_0,     KC_MINS,                 KC_DEL,   KC_PGUP,   KC_NO,   KC_PGDN,  KC_NO,    A(BP_PERC),
                                    DM_REC1, DM_REC2,                                                          DM_PLY1, DM_PLY2,
                                                       OSM(MOD_LCTL),  OSM(MOD_LSFT),                 KC_TRNS,  KC_TRNS,
                                                       OSM(MOD_LALT),  OSM(MOD_LGUI),                 KC_TRNS,  KC_TRNS,
                                                       OSM(MOD_RALT),  KC_TRNS,                 KC_TRNS,  KC_TRNS
               ),                                                                                                                           
  [KL_MOUSE_AND_FCT] = LAYOUT(  // Mouse & functions                                                        
               KC_TRNS,  KC_F1,     KC_F2,   KC_F3,    KC_F4,    KC_F5,                   KC_MAIL,  KC_BTN1,   KC_MS_U, KC_BTN2,  KC_WH_U,  C(KC_PGUP),
               KC_TRNS,  KC_F6,     KC_F7,   KC_F8,    KC_F9,    KC_F10,                  KC_WHOM,  KC_MS_L,   KC_MS_D, KC_MS_R,  KC_WH_D,  C(BP_W),
               KC_TRNS,  KC_F11,    KC_F12,  KC_F13,   KC_F14,   KC_F15,                  KC_NO  ,  KC_BTN3,   KC_NO,   KC_BTN4,  KC_NO,    C(KC_PGDN),
                                    KC_BTN1, KC_BTN2,                                                          KC_TRNS, TO(KL_NUMBER_AND_FCT),
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS
                 ),
  [KL_OSL] = LAYOUT(  // OSL                                                      
               KC_TRNS,  KC_1,      KC_2,    KC_3,     KC_4,     KC_5,                    KC_6,     KC_7,      KC_8,    KC_9,     KC_0,     KC_EQL,
               KC_NO,    KC_NO,     KC_NO,   KC_NO,    KC_NO,    KC_NO,                   KC_NUHS,  KC_4,      ALGR(KC_X),   ALGR(KC_4),    ALGR(KC_2),  KC_NO,
               KC_ASDN,  KC_ASUP,   KC_ASRP, DF(KL_BASE),    DF(KL_QWERTY_SHIFTED),    DF(KL_QWERTY),           KC_1,     KC_5,      ALGR(KC_C),   ALGR(KC_5),    ALGR(KC_3),  S(KC_EQL),
                                    KC_ASON,  KC_ASOFF,                                                        A(BP_A),  A(BP_V),
                                                       G(KC_1),  G(KC_2),                 G(KC_8),  G(KC_7),
                                                       G(KC_6),  G(KC_3),                 G(KC_9),  KC_NO,
                                                       G(KC_5),  G(KC_4),                 G(KC_0),  G(KC_0)
                      )
};

// Activate numlock
// void led_set_user(uint8_t usb_led) {
//   if (!(usb_led & (1<<USB_LED_NUM_LOCK))) {
//     tap_code(KC_NLCK);
//   }
// }
  
// void persistent_default_layer_set(uint16_t default_layer) {
//     eeconfig_update_default_layer(default_layer);
//     default_layer_set(default_layer);
// }


LEADER_EXTERNS();
void matrix_scan_user(void) {
  LEADER_DICTIONARY() {
    leading = false;
    leader_end();

    // quote -> cut sound and video in Zoom
    SEQ_ONE_KEY(BP_QUOT) {
      register_code(KC_LALT);
      register_code(BP_A);
      unregister_code(BP_A);
      unregister_code(KC_LALT);
      register_code(KC_LALT);
      register_code(BP_V);
      unregister_code(BP_V);
      unregister_code(KC_LALT);
    }

    // Q -> save in emacs
    SEQ_ONE_KEY(BP_Q) {
      register_code(KC_LCTL);
      register_code(BP_X);
      unregister_code(BP_X);
      register_code(BP_S);
      unregister_code(BP_S);
      unregister_code(KC_LCTL);
    }

    SEQ_ONE_KEY(BP_C) { 
      SEND_STRING("\"\"" SS_TAP(X_LEFT));
    }

    SEQ_ONE_KEY(BP_T) {
      SEND_STRING("()" SS_TAP(X_LEFT));
    }

    SEQ_ONE_KEY(BP_S) {
      SEND_STRING("{}" SS_TAP(X_LEFT));
    }

    SEQ_ONE_KEY(BP_R) {
      SEND_STRING("[]" SS_TAP(X_LEFT));
    }

    // P I
    SEQ_TWO_KEYS(BP_P, BP_I) {
      SEND_STRING("import ipdb; ipdb.set_trace()");
    }

    // P W
    SEQ_TWO_KEYS(BP_P, BP_W) {
      SEND_STRING("import wdb; wdb.set_trace()");
    }

    // E X
    SEQ_TWO_KEYS(BP_E, BP_X) {
      SEND_STRING("norman@xael.org");
    }
    // E H
    SEQ_TWO_KEYS(BP_E, BP_H) {
      SEND_STRING("alexandre.norman@hespul.org");
    }
  }
}


