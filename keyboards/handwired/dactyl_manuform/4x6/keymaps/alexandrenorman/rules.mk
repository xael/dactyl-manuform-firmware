# MCU name
MCU = atmega32u4

# Bootloader selection
# BOOTLOADER = caterina
# BOOTLOADER = atmel-dfu
BOOTLOADER = qmk-dfu

# Build Options
#   change yes to no to disable
#
BOOTMAGIC_ENABLE = no       # Enable Bootmagic Lite
MOUSEKEY_ENABLE = yes       # Mouse keys
EXTRAKEY_ENABLE = no       # Audio control and System control
CONSOLE_ENABLE = no         # Console for debug
COMMAND_ENABLE = no        # Commands for debug and configuration
NKRO_ENABLE = no            # Enable N-Key Rollover
BACKLIGHT_ENABLE = no       # Enable keyboard backlight functionality
RGBLIGHT_ENABLE = no        # Enable keyboard RGB underglow
AUDIO_ENABLE = no           # Audio output
SPLIT_KEYBOARD = yes

FORCE_NKRO = no
COMBO_ENABLE = no
TAP_DANCE_ENABLE = yes

OLED_ENABLE = yes
OLED_DRIVER = SSD1306
# WPM_ENABLE = yes

AUTO_SHIFT_ENABLE = yes
AUTO_SHIFT_MODIFIERS = yes

CONSOLE_ENABLE = no
COMMAND_ENABLE = no


SPACE_CADET_ENABLE = no
GRAVE_ESC_ENABLE = no 
MAGIC_ENABLE = no

MUSIC_ENABLE = no

DYNAMIC_MACRO_ENABLE = yes
LEADER_ENABLE = yes

SLEEP_LED_ENABLE = no
BACKLIGHT_ENABLE = no
RGBLIGHT_ENABL = no
MIDI_ENABLE	= no
# UNICODE_ENABLE = no
BLUETOOTH_ENABLE = no
AUDIO_ENABLE = no
FAUXCLICKY_ENABLE = no
HD44780_ENABLE = no

EXTRAFLAGS += -flto
